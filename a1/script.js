console.log('Hello World');

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	level: "Strong",
	pokemon: "Pikachu",
}

console.log(trainer);

function Pokemon(name){
	this.trainer = trainer;
	this.name = name;
	this.health = 100;
	this.level = "strong";
	this.attack = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		target.health -=10;
		console.log(`${this.name}'s health is now ${target.health}`);
	}
};
let Pikachu = new Pokemon("Pikachu");
let Meowth = new Pokemon("Meowth");

Pikachu.attack(Meowth);